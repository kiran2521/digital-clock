const hours = document.querySelector('.hours');
const mins = document.querySelector('.mins');
const sec = document.querySelector('.sec');

setInterval(()=>{
    let now = new Date();
    hours.innerHTML = String(now.getHours()).padStart(2,0);
    mins.innerHTML = String(now.getMinutes()).padStart(2,0);
    sec.innerHTML = String(now.getSeconds()).padStart(2,0);
},1000 * 1);